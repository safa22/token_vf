RedToken

C'est un projet réaliser au sein de l'entreprise redlean pour améliorer la performance de ses employés.

Technologie utilisée:

- Ethereum
- Solidity
- ERC-20 Token
- Web3.js
- truffle.js
- ganache / ganache-cli 
- node.js
- jQuery/ ajax


Installation:


 1) installer:  node & npm
 
                     npm install -g truffle
                     npm install -g ganache-cli
                 
 2) git clone :https://github.com/trufflesuite/ganache-cli.git
 3) cd ganche-cli 
 4) npm install
 5) pour la modification des propriéter du réseau ethereum : args.js
 6) forever start cli.js
 7) git clone : https://safa22@bitbucket.org/safa22/redlean_wallet-v2.git 
 8) cd redlean_wallet-v2 
 9) npm install
 10) truffle compile
 10) truffle migrate
 10) forever start server.js


 2) Structure du répertoire

La structure de répertoire par défaut de Truffle contient les éléments suivants:

contracts/: Contient les fichiers source Solidity pour nos contrats intelligents. Il y a un contrat important ici appelé Migrations.sol, dont nous parlerons plus tard.

migrations/: Truffle utilise un système de migration pour gérer les déploiements de contrats intelligents. Une migration est un contrat intelligent spécial.
supplémentaire qui assure le suivi des modifications.

test/: Contient les tests JavaScript et Solidity pour nos contrats intelligents.

truffle.js: Fichier de configuration de truffe.




node cli.js -i="5777" --mnemonic="tattoo track warrior surface coyote jewel brick motion pull donkey embody ribbon"













