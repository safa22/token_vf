const express = require('express');
const app = express();
const port = 3000 || process.env.PORT;
const Web3 = require('web3');
const truffle_connect = require('./connection/app.js');
const bodyParser = require('body-parser');
var response = new Array();



app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());


app.use('/', express.static('public_static'));

app.get('/getAccounts', (req, res) => {
  console.log("**** GET /getAccounts ****");
  truffle_connect.start(function (answer) {
    res.send(answer);
    //console.log(answer);

  })
});

app.post('/getBalance', (req, res) => {
  console.log("**** GET /getBalance ****");
  //console.log(req.body);
  let currentAcount = req.body.account;

  truffle_connect.refreshBalance(currentAcount, (answer) => {
    let account_balance = answer;
    truffle_connect.start(function (answer) {
      // get list of all accounts and send it along with the response
      let all_accounts = answer;
      response = [account_balance, all_accounts]

      res.send(response);
    });
  });
});


app.post('/chekBalance', (req, res) => {
  //  console.log("**** GET /chekBalance ****");
  //console.log(req.body.account);
  let currentAcount = req.body.account;

  truffle_connect.refreshBalance(currentAcount, (answer) => {
   
    let account_balance = answer;
    truffle_connect.start(function (answer) {


      res.send(account_balance);
    })
  });

});


app.post('/sendCoin', (req, res) => {
  console.log("**** GET /sendCoin ****");
  // console.log(req.body);

  let amount = req.body.amount;
  let sender = req.body.sender;
  let receiver = req.body.receiver;

  truffle_connect.sendCoin(amount, sender, receiver, (balance) => {
    res.send(balance);
  });
});


app.post('/transaction', (req, res) => {
  console.log("**** GET /transaction ****");
 
  truffle_connect.transaction(function (answer) {
     console.log(answer);
    res.send(answer);
  });
   
});





app.listen(port, () => {

  // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
  truffle_connect.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

  console.log("Express Listening at http://localhost:" + port);

});
