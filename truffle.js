// Allows us to use ES6 in our migrations and tests.
require('babel-register')

module.exports = {
  networks: {
    development: {
      host: '127.0.0.1',
      port: 8545,
      network_id: '*',
      networkName: "ganache",
      networkType: "testrpc",
     // token: {
       // tokenContractAddress: "0x6803674615ec58c0ac76b5cba13882e8ab71e60d",
       // migrateContractAddress: "0x37bf2be9280a2b239b1da27181a57070ff919232"
     // },
    }
  }
}
